#!/usr/bin/env python
import setuptools
import sys

with open("README.md", "r") as fh:
    long_description = fh.read()

version = '0.1.5.1'
args = sys.argv
if('--version' in args):
      print(version)
      exit(1)

package_dir = {
      'influxed': 'src/influxed', 
      'influxed.ifql': 'src/influxed/ifql',
      'influxed.ifql.line_protocol': 'src/influxed/ifql/line_protocol',
      'influxed.mediator': 'src/influxed/mediator',
      'influxed.mediator.client_wrappers': 'src/influxed/mediator/client_wrappers',
      'influxed.orm': 'src/influxed/orm',
      'influxed.orm.capabilities': 'src/influxed/orm/capabilities',
      'influxed.orm.columns': 'src/influxed/orm/columns',
      'influxed.orm.declarative': 'src/influxed/orm/declarative'
}
setuptools.setup(
      name='influxed',
      version=version,
      author='Emil S Roemer',
      author_email='emilromer@hotmail.com',
      description='Inlfuxed influx query language and orm',
      package_dir=package_dir,
      long_description=long_description,
      long_description_content_type="text/markdown",
      url='https://gitlab.com/Romeren/influxed/',
      # py_modules=['influxed.__init__'],
      packages=[k for k, _ in package_dir.items()],
      install_requires=['pandas', 'requests'],
      keywords='Influx InfluxDb Query Language QL ORM',
      classifiers=[
            'Development Status :: 3 - Alpha',
            'Intended Audience :: Developers',
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+) ",
            "Operating System :: OS Independent",
      ],
)