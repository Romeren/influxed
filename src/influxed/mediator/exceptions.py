#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: exceptions.py
 File Created: Tuesday, 9th April 2019 2:15:29 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""



class ActionNotAllowed(Exception):
    pass


