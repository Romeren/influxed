#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: exceptions.py
 File Created: Thursday, 11th April 2019 6:58:29 pm
 Author: ESR - Romeren (emil@spectral.energy)
 -----
 Copyright 2019 Spectral, Spectral
 -----
 Last Modified:
 Date	By	Comments
 -----
"""



class MissingArgument(AttributeError):
    pass
