#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: __init__.py
 File Created: Monday, 25th February 2019 8:08:22 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
name = "influxed.orm"
from influxed.orm.engine import engine