#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: database.py
 File Created: Monday, 25th February 2019 8:14:31 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
from inspect import isclass
from influxed.ifql.column import Tag
from influxed.orm.capabilities.showable import Showable
from influxed.orm.capabilities.executable import Executable
from influxed.orm.capabilities.queryable import Queryable
from influxed.orm.capabilities.insertable import Insertable
from influxed.orm.capabilities.asyncable import HandlePosibleAsync
from influxed.orm.rp import RetentionPolicy
from influxed.ifql.util import KEY_WORDS


class Database(Showable, Insertable, Queryable, Executable):
    """
    Influx definition of a database instance
    """

    @property
    def name(self):
        return self.__db__

    @name.setter
    def name(self, value):
        self.__db__ = value

    @property
    def rps(self):
        if not hasattr(self, "__rps__"):
            self.__rps__ = {}
        return self.__rps__

    @rps.setter
    def rps(self, value):
        self.__rps__ = value

    @property
    def database(self):
        return self

    @property
    def influx_server(self):
        return self.__influx_server__

    @influx_server.setter
    def influx_server(self, server):
        self.__influx_server__ = server

    @property
    def connection_string(self):
        return self.influx_server.connection_string

    @property
    def username(self):
        return self.influx_server.username

    @property
    def mediator(self):
        return self.influx_server.mediator

    def __show_prefix__(self, show_statement):
        """
        Overwrite from showable
        """
        return show_statement.on(self.name)

    def __insert_prefix__(self, insert_statement):
        return insert_statement.on(self.name)

    def __getattr__(self, name):
        if name in ("__rps__"):
            raise AttributeError
        if name in self.rps:
            return self.rps[name]
        self.__add_orm_rp_from_name__(name)
        return self.rps[name]

    def __getitem__(self, item):
        return self.__getattr__(item)

    def reflect(self):
        # Get and reflect rps:
        possible_future = HandlePosibleAsync(
            self.show(KEY_WORDS.RETENTION_POLICIES).on(self.database.name).all()
        )
        possible_future.chain_function(self.__create_orm_rps__)
        possible_future.chain_function(self.__nested_rp_reflect__)
        return possible_future.return_()

    def __nested_rp_reflect__(self, rps):
        possible_futures = []
        for rp in rps.values():
            possible_futures.append(rp.reflect())
        return possible_futures

    def __create_orm_rps__(self, rps):
        if rps.empty:
            return self.rps
        for name in rps.name:
            self.__add_orm_rp_from_name__(name)
        return self.rps

    def __add_orm_rp_from_name__(self, name):
        if name in self.rps:
            return

        m = RetentionPolicy().set_database(self)
        m.name = name
        self.__add_orm_rp__(m)

    def __add_orm_rp__(self, rp):
        self.rps[rp.name] = rp
        return self

    def ls(self, all=False, prefix=""):
        for k, v in self.rps.items():
            print(f"{prefix} RetentionPolicy {k}")
            if all:
                v.ls(prefix=prefix + "  ")
