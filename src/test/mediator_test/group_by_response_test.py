#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: group_by_response_test.py
 File Created: Monday, 15th April 2019 11:53:59 am
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import unittest
import json
from influxed.mediator.response_parser import DataframeParser

class GroupTest(unittest.TestCase):


    def test_group_by_parsing(self):
        test_data = {
            "results":[
                {
                    "statement_id":0,
                    "series":[
                        {
                            "name":"weather",
                            "tags": {"id":"Drenthe"},
                            "columns":["time","temperature"],
                            "values":[ ["2017-11-04T23:00:00Z",51.63],["2017-11-05T00:00:00Z",50.78]]
                        },{ 
                            "name":"weather",
                            "tags":{"id":"Drenthe_f"},
                            "columns":["time","temperature"],
                            "values":[["2018-12-05T23:00:00Z",44.49],["2018-12-06T00:00:00Z",44.49]]
                        },{
                            "name":"weather",
                            "tags":{"id":"Eindhoven"},
                            "columns":["time","temperature"],
                            "values":[["1970-01-01T00:00:01.5531228Z",7.72222],["1970-01-01T00:00:01.5531264Z",6.71111]]
                        }
                    ]
                }
            ]
        }
        test_response = lambda: None
        test_response.body = json.dumps(test_data)
        test_response.body = test_response.body.encode()
        
        df = DataframeParser().parse_response(test_response)
        
        self.assertTrue(len(list(df.columns)) == 3)
        asserts = [x in ['temperature-Drenthe', 'temperature-Drenthe_f', 'temperature-Eindhoven'] for x in list(df.columns)]
        self.assertTrue(all(asserts))
