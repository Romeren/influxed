#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: multi_http_lib_test.py
 File Created: Sunday, 24th February 2019 7:46:31 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import asyncio
import os
import requests
from tornado.httpclient import HTTPClient, AsyncHTTPClient
import unittest
from influxed.mediator.mediator import Mediator


class MultiLibTest(unittest.TestCase):
    """
        Important, these tests only run if you have a connection string, username and a password
        in your env.
    """

    def test_requests_lib(self):
        mediator = Mediator()
        mediator.add_client(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            client=requests
        )
        self.assertEqual(mediator.number_of_clients, 1)
        self.assertIsNotNone(mediator.get_client(
            os.getenv('connection_string'),
            os.getenv('username'),
        ))
        res = mediator.execute('SHOW DATABASES')
        self.assertIsNotNone(
            res,
            f'SHOW DATABASES RETURNED {res} | USED {os.getenv("connection_string")} and {os.getenv("username")}'
        )

    def test_tornado_lib(self):
        mediator = Mediator()
        mediator.add_client(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            client=HTTPClient
        )
        self.assertEqual(mediator.number_of_clients, 1)
        self.assertIsNotNone(mediator.get_client(
            os.getenv('connection_string'),
            os.getenv('username'),
        ))
        res = mediator.execute('SHOW DATABASES')
        self.assertIsNotNone(
            res,
            f'SHOW DATABASES RETURNED {res} | USED {os.getenv("connection_string")} and {os.getenv("username")}'
        )
    
    def test_tornado_async(self):
        mediator = Mediator()
        mediator.add_client(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            client=AsyncHTTPClient
        )
        self.assertEqual(mediator.number_of_clients, 1)
        self.assertIsNotNone(mediator.get_client(
            os.getenv('connection_string'),
            os.getenv('username'),
        ))
        res = mediator.execute('SHOW DATABASES')
        print(res)
        self.assertTrue(
            asyncio.iscoroutine(
                res
            )
        )


