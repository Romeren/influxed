#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: multi_http_lib_test.py
 File Created: Sunday, 24th February 2019 7:46:31 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import asyncio
import os

import requests
from tornado.httpclient import HTTPClient, AsyncHTTPClient
import unittest
from influxed.mediator.mediator import Mediator
from influxed.mediator.exceptions import ActionNotAllowed

import datetime as dt
from influxed import engine, time


class ErrorsTest(unittest.TestCase):
    """
        Important, these tests only run if you have a connection string, username and a password
        in your env.
    """

    def test_connection_refused(self):
        class ConnectionRefusedErrorClient():

            def fetch(self, url, body, method, headers, **request_parameters):
                raise ConnectionRefusedError
        
        mediator = Mediator()
        mediator.add_client(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            client=ConnectionRefusedErrorClient
        )

        with self.assertRaises(ConnectionRefusedError):
            mediator.execute('SHOW DATABASES')

    def test_os_exception(self):
        class ErrnoClient():

            def fetch(self, url, body, method, headers, **request_parameters):
                raise OSError(101)
        
        mediator = Mediator()
        mediator.add_client(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            client=ErrnoClient
        )

        with self.assertRaises(OSError):
            mediator.execute('SHOW DATABASES')

    def test_http_403(self):
        class Http403ErrorClient():

            def fetch(self, url, body, method, headers, **request_parameters):
                e = Exception()
                e.code = 403
                raise e
        mediator = Mediator()
        mediator.add_client(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            client=Http403ErrorClient
        )

        with self.assertRaises(ActionNotAllowed):
            mediator.execute('SHOW DATABASES')
    
    def test_http_401(self):
        class Http401ErrorClient():

            def fetch(self, url, body, method, headers, **request_parameters):
                e = Exception()
                e.code = 401
                raise e
        mediator = Mediator()
        mediator.add_client(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            client=Http401ErrorClient
        )

        with self.assertRaises(ActionNotAllowed):
            mediator.execute('SHOW DATABASES')
