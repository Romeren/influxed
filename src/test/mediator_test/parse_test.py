#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: parse_test.py
 File Created: Monday, 15th April 2019 9:45:52 am
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import asyncio
import unittest
import msgpack
import pandas as pd
from influxed.mediator.response_parser import StringParse, JsonParse, MsgpackParse, DataframeParser


class ParseTest(unittest.TestCase):
    """
        Important, these tests only run if you have a connection string, username and a password
        in your env.
    """

    def test_parse_to_string(self):
        test = lambda: None
        test.body = '{"results":[{"statement_id":0,"series":[{"name":"databases","columns":["name"],"values":["Test1", "Test2"]}]}]}'.encode()
        res = StringParse().parse_response(test)
        self.assertIsInstance(res, str)
        self.assertEqual(test.body.decode(), res)
    

    def test_parse_from_json(self):
        test = lambda: None
        test.body = '{"results":[{"statement_id":0,"series":[{"name":"databases","columns":["name"],"values":["Test1", "Test2"]}]}]}'.encode()
        res = JsonParse().parse_response(test)
        self.assertIsInstance(res, dict)
        self.assertTrue('results' in res)
        self.assertIsInstance(res['results'], list)
        self.assertTrue('statement_id' in res['results'][0])
        self.assertTrue('series' in res['results'][0])
        self.assertIsInstance(res['results'][0]['series'], list)
        self.assertTrue('name' in res['results'][0]['series'][0])
        self.assertTrue('columns' in res['results'][0]['series'][0])
        self.assertTrue('values' in res['results'][0]['series'][0])


    def test_parse_to_dataframe(self):
        test = lambda: None
        test.body = '{"results":[{"statement_id":0,"series":[{"name":"databases","columns":["name"],"values":["Test1", "Test2"]}]}]}'.encode()
        res = DataframeParser().parse_response(test)
        self.assertIsInstance(res, pd.DataFrame)
        self.assertTrue("name" in res.columns)

    

    def test_parse_from_msgpack(self):
        test = lambda: None
        test.body = msgpack.packb(
            {
                "results":[
                    { 
                        "statement_id":0,
                        "series": [
                            {
                                "name":"databases",
                                "columns": ["name"],
                                "values": ["Test1", "Test2"]
                            }
                        ]
                    }
                ]
            }, use_bin_type=True
        )
        res = MsgpackParse().parse_response(test)
        self.assertTrue('results' in res)
        self.assertIsInstance(res['results'], (list, tuple))
        self.assertTrue('statement_id' in res['results'][0])
        self.assertTrue('series' in res['results'][0])
        self.assertIsInstance(res['results'][0]['series'], (list, tuple))
        self.assertTrue('name' in res['results'][0]['series'][0])
        self.assertTrue('columns' in res['results'][0]['series'][0])
        self.assertTrue('values' in res['results'][0]['series'][0])