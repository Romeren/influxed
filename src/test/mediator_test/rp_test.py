#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: rp_test.py
 File Created: Friday, 19th July 2024 04:41:00 pm
 Author: fakegermano - Daniel Travieso (danielgtravieso@gmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import os
import unittest
import requests
from influxed.mediator.mediator import Mediator
from influxed.ifql import show, insert, select


class RpTest(unittest.TestCase):
    def setUp(self):
        self.mediator = Mediator()
        self.mediator.add_client(
            os.getenv("connection_string"),
            os.getenv("username"),
            os.getenv("password"),
            client=requests,
        )

    def test_show_no_rp_provided(self):
        query = show().measurements.on("test")
        self.assertEqual(query.database, "test")
        self.assertIsNone(query.rp)
        res = self.mediator.execute(query)

    def test_show_rp_provided(self):
        query = show().measurements.on("test", rp="autogen")
        self.assertEqual(query.database, "test")
        self.assertEqual(query.rp, "autogen")
        res = self.mediator.execute(query)

    def test_insert_no_rp_provided(self):
        query = insert(data={"test": 0}).measurement("msmt").on("test")
        self.assertEqual(query.database, "test")
        self.assertIsNone(query.rp)
        res = self.mediator.execute(query)

    def test_insert_rp_provided(self):
        query = insert(data={"test": 0}).measurement("msmt").on("test", rp="autogen")
        self.assertEqual(query.database, "test")
        self.assertEqual(query.rp, "autogen")
        res = self.mediator.execute(query)

    def test_select_no_rp_provided(self):
        query = select().from_("msmt").on("test")
        self.assertEqual(query.database, "test")
        self.assertIsNone(query.rp)
        res = self.mediator.execute(query)

    def test_select_rp_provided(self):
        query = select().from_("msmt").on("test", rp="autogen")
        self.assertEqual(query.database, "test")
        self.assertEqual(query.rp, "autogen")
        res = self.mediator.execute(query)
