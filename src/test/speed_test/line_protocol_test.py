#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: line_protocol_test.py
 File Created: Thursday, 11th April 2019 7:05:05 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
from test.speed_test.aspeed_test import speed_test
from influxed.ifql.line_protocol.list_to_line import to_lines
import time

class line_protocol_test(speed_test):
    min_size = 10000
    max_size = 100000
    step_interval = 10000
    
    def make_dataset(self, current_size):
        return [dict(time=x, value=x, tag=str(x)) for x in range(current_size)]

    def timed_function(self, dataset):
        _ = to_lines(dataset, measurement='test')
            

