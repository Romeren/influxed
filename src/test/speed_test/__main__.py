#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: __main__.py
 File Created: Thursday, 11th April 2019 8:04:58 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

from test.speed_test import tests


for clss in tests:
    print('=================================================')
    print('Run:', clss.__name__)
    timetable = clss().run()
    xes = []
    ys = []
    diffs = []
    for indx, (x, y) in enumerate(timetable):
        xes.append('{:7}'.format(round(x, 1)))
        ys.append('{:,.5f}'.format(round(y, 5)))
        if(indx > 0):
            diffs.append('{:,.5f}'.format(round(y - timetable[indx-1][1], 5)))
        else:
            diffs.append('{:,.5f}'.format(0))

    print('X   :', xes)
    print('Y   :', ys)
    print('DIFF:', diffs)
    
