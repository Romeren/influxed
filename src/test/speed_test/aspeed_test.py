#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: aspeed_test.py
 File Created: Thursday, 11th April 2019 7:44:41 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import time

class speed_test(object):
    run_per_size = 5
    min_size = 1
    max_size = 10
    step_interval = 1

    def run(self):
        timetable = []
        for x in range(self.min_size, self.max_size, self.step_interval):
            timetable.append((
                x,
                self.take_time(
                    self.make_dataset(x)
                )
            ))
        return timetable

    def make_dataset(self, current_size):
        return None

    def timed_function(self, dataset):
        raise NotImplementedError
    
    def take_time(self, dataset):
        timetable = []
        for _ in range(self.run_per_size):
            t0 = time.time()
            _ = self.timed_function(dataset)
            timetable.append(time.time() - t0)
        return sum(timetable) / self.run_per_size
