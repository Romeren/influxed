#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: __init__.py
 File Created: Thursday, 11th April 2019 7:41:40 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

from test.speed_test.aspeed_test import speed_test
# import test.speed_test.line_protocol_test
import test.speed_test.generic_test


tests = speed_test.__subclasses__()

