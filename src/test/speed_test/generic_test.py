#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: generic_test.py
 File Created: Thursday, 11th April 2019 8:22:03 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import asyncio
import time
import os
import datetime as dt
import requests
from tornado.httpclient import HTTPClient, AsyncHTTPClient
from test.speed_test.aspeed_test import speed_test
from influxed.orm.engine import Engine


class mock_io_client(object):
    return_object = None

    def __init__(self, *arg, **args):
        pass
    
    def fetch(self, url, body, method, headers, **request_parameters):
        return mock_io_client_response_wrapper(self.return_object)

class mock_io_client_response_wrapper(object):

    def __init__(self, res):
        self.body = res.encode()

class show_databases_test(speed_test):
    min_size = 100
    max_size = 1000
    step_interval = 100
    
    def make_dataset(self, current_size):
        e = Engine()

        dbs = []
        for x in range(current_size):
            dbs.append(f'["db-{x}"]')
        
        dbs = ','.join(dbs)
        res = '{"results":[{"statement_id":0,"series":[{"name":"databases","columns":["name"],"values":['+ dbs +']}]}]}'
        
        mock_io_client.return_object = res
        e.add_server('whatever', 'stuff', 'more', reflect=False, client=mock_io_client)
        return e

    def timed_function(self, engine):
        engine.server.show().databases.all()

class select_statement_test(speed_test):
    min_size = 10000
    max_size = 100000
    step_interval = 10000
    
    def make_dataset(self, current_size):
        e = Engine()

        columns = ['"time"', '"a"', '"b"']
        values = []
        for x in range(current_size):
            row = [
                str(int((dt.datetime.now() - dt.timedelta(hours=x)).timestamp() * 1e9)),
                str(1),
                str(int(x))
            ]
            values.append('[' + ','.join(row) + ']')
        
        res = '{"results":[{"statement_id":0,"series":[{"name":"meterusage","columns":[' + ','.join(columns) + '],"values":[' + ','.join(values) + ']}]}]}'
        
        mock_io_client.return_object = res
        e.add_server('whatever', 'stuff', 'more', reflect=False, client=mock_io_client)
        return e

    def timed_function(self, engine):
        engine.server.execute('SELECT * FROM meterusage')

class requests_reflect_server_sync_test(speed_test):
    min_size = 1
    max_size = 2
    step_interval = 1
    
    def make_dataset(self, current_size):
        e = Engine()
        return e

    def timed_function(self, engine):
        engine.add_server(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            reflect=True,
            client=requests
        )

class tornado_reflect_server_sync_test(speed_test):
    min_size = 1
    max_size = 2
    step_interval = 1
    
    def make_dataset(self, current_size):
        e = Engine()
        return e

    def timed_function(self, engine):
        engine.add_server(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            reflect=True,
            client=HTTPClient
        )

class tornado_reflect_server_async_test(speed_test):
    min_size = 1
    max_size = 2
    step_interval = 1
    loop = asyncio.get_event_loop()
    
    def make_dataset(self, current_size):
        e = Engine()
        return e

    def timed_function(self, engine):
        res = self.loop.run_until_complete(
            engine.add_server(
                os.getenv('connection_string'),
                os.getenv('username'),
                os.getenv('password'),
                reflect=True,
                client=AsyncHTTPClient
            )
        )
