#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: show_test.py
 File Created: Sunday, 17th February 2019 7:32:22 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import unittest
from influxed import select


class SubQueryTest(unittest.TestCase):

    def test_show_database(self):
        sub = select('a').from_('b')
        parent = select('a').from_(sub)
        query = parent.format()
        self.assertEqual(query, 'SELECT a FROM (SELECT a FROM b)')
        

