#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: show_test.py
 File Created: Sunday, 17th February 2019 7:32:22 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import unittest
from influxed import show
from influxed import KEY_WORDS


class SelectTest(unittest.TestCase):

    def test_show_database(self):
        res = show(KEY_WORDS.DATABASES).format()
        self.assertEqual(res, 'SHOW DATABASES')

    def test_show_measurements(self):
        res = show(KEY_WORDS.MEASUREMENTS).format()
        self.assertEqual(res, 'SHOW MEASUREMENTS')
    
    def test_show_measurements_from(self):
        res = show(KEY_WORDS.MEASUREMENTS).from_('test').format()
        self.assertEqual(res, 'SHOW MEASUREMENTS FROM test')
    
    def test_show_retention_policies(self):
        res = show(KEY_WORDS.RETENTION_POLICIES).format()
        self.assertEqual(res, 'SHOW RETENTION POLICIES')
    
    def test_show_tag_keys(self):
        res = show(KEY_WORDS.TAG_KEYS).format()
        self.assertEqual(res, 'SHOW TAG KEYS')
    
    def test_show_field_keys(self):
        res = show(KEY_WORDS.FIELD_KEYS).format()
        self.assertEqual(res, 'SHOW FIELD KEYS')
    
    def test_show_series(self):
        res = show(KEY_WORDS.SERIES).format()
        self.assertEqual(res, 'SHOW SERIES')
    
    def test_show_users(self):
        res = show(KEY_WORDS.USERS).format()
        self.assertEqual(res, 'SHOW USERS')

    def test_show_awesome_syntax(self):
        res = show().users.format()
        res = show().series.format()
        res = show().field_keys.format()
        res = show().tag_keys.format()
        res = show().retention_policies.format()
        res = show().measurements.format()
        res = show().databases.format()