#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: insert_test.py
 File Created: Wednesday, 20th February 2019 7:38:18 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import unittest
import datetime as dt
import pandas as pd
from influxed import insert
from influxed.ifql.exceptions import MissingArgument


class InsertTest(unittest.TestCase):

    def test_insert_missing_argument_db(self):
        with self.assertRaises(MissingArgument):
            insert({"test_field": 1}).measurement("testmeasurement").format()

    def test_insert_missing_argument_measurement(self):
        with self.assertRaises(MissingArgument):
            insert({"test_field": 1}).on("testdb").format()

    def test_insert_with_rp(self):
        now = dt.datetime.now()
        query = (
            insert({"test_field": 1, "time": now})
            .measurement("testmeasurement")
            .on("testdb", rp="autogen")
        )
        self.assertEqual(query.rp, "autogen")

    def test_insert_dict(self):
        now = dt.datetime.now()
        res = (
            insert({"test_field": 1, "time": now})
            .measurement("testmeasurement")
            .on("testdb")
            .format()
        )
        self.assertEqual(
            res, f"testmeasurement test_field=1.00000 {int(now.timestamp() * 10e8)}"
        )

    def test_insert_dict_time_int(self):
        now = dt.datetime.now().timestamp()
        res = (
            insert({"test_field": 1, "time": now})
            .measurement("testmeasurement")
            .on("testdb")
            .format()
        )
        now = int(now)
        self.assertEqual(res, f"testmeasurement test_field=1.00000 {now}")

    def test_insert_single_value(self):
        res = insert(test_field=1).measurement("testmeasurement").on("testdb").format()
        self.assertTrue(res.startswith("testmeasurement test_field=1.00000 "))

    def test_insert_multi_value(self):
        res = (
            insert(test_field=1, test_field2=2)
            .measurement("testmeasurement")
            .on("testdb")
            .format()
        )
        self.assertTrue(
            res.startswith("testmeasurement test_field=1.00000,test_field2=2.00000 "),
            res,
        )

    def test_insert_list_of_values(self):
        now = dt.datetime.now().timestamp()
        res = (
            insert([{"test_field": 1, "time": now}, {"test_field": 2}])
            .measurement("testmeasurement")
            .on("testdb")
            .format_lines()
        )
        self.assertTrue(len(res) == 2, f"{len(res)} != 2")
        now = int(now)
        self.assertEqual(res[0], f"testmeasurement test_field=1.00000 {now}")
        self.assertTrue(res[1].startswith("testmeasurement test_field=2.00000 "), res)

    def test_insert_df(self):
        date_range = pd.date_range(
            dt.datetime(2011, 1, 1), dt.datetime(2011, 2, 1), freq="D"
        )
        df = pd.Series(1, date_range).to_frame()
        df.columns = ["test_field"]
        res = insert(df).measurement("testmeasurement").on("testdb").format_lines()
        self.assertTrue(len(res) == df.shape[0], f"{len(res)} != {df.shape[0]}")
        for x in range(df.shape[0]):
            timestamp = int(date_range[x].timestamp() * 10e8)
            self.assertTrue(
                res[x].startswith(f"testmeasurement test_field=1.00000 {timestamp}"),
                res,
            )

    def test_insert_with_nan(self):
        date_range = pd.date_range(
            dt.datetime(2011, 1, 1), dt.datetime(2011, 2, 1), freq="D"
        )
        df = pd.Series(1, date_range).to_frame()
        df2 = pd.Series(pd.np.nan, date_range).to_frame()
        df = pd.concat([df, df2], axis=1)

        df.columns = ["test_field", "Nans"]

        res = insert(df).measurement("testmeasurement").on("testdb").format_lines()

        self.assertTrue(len(res) == df.shape[0], f"{len(res)} != {df.shape[0]}")
        for x in range(df.shape[0]):
            timestamp = int(date_range[x].timestamp() * 10e8)
            self.assertTrue(
                res[x].startswith(f"testmeasurement test_field=1.00000 {timestamp}"),
                res,
            )

    def test_insert_with_only_nan(self):
        date_range = pd.date_range(
            dt.datetime(2011, 1, 1), dt.datetime(2011, 2, 1), freq="D"
        )
        df = pd.Series(pd.np.nan, date_range).to_frame()

        df.columns = ["Nans"]

        res = insert(df).measurement("testmeasurement").on("testdb").format_lines()

        self.assertTrue(len(res) == 0)

        df.iloc[-1] = 1
        res = insert(df).measurement("testmeasurement").on("testdb").format_lines()
        self.assertTrue(len(res) == 1)
