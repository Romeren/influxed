#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: grant_test.py
 File Created: Thursday, 21st February 2019 10:34:47 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import unittest
from influxed import grant
from influxed.ifql.util import PRIVILEGES
from influxed.ifql.exceptions import MissingArgument


class GrantTest(unittest.TestCase):
    """
        GRANT ALL PRIVILEGES TO <username>
        GRANT [READ,WRITE,ALL] ON <database_name> TO <username>
    """

    def test_grant_privileges_missing_argument_user(self):
        with self.assertRaises(MissingArgument):
            grant(PRIVILEGES.ALL).format()
    
    def test_grant_privileges_all(self):
        res = grant(PRIVILEGES.ALL).to('Emil').format() # Muhahah...!
        self.assertEqual(res, 'GRANT ALL PRIVILEGES TO Emil')

    def test_grant_privileges_all_database(self):
        res = grant(PRIVILEGES.ALL).on('db').to('Emil').format()
        self.assertEqual(res, 'GRANT ALL ON db TO Emil')

    def test_grant_privileges_read(self):
        res = grant(PRIVILEGES.READ).on('db').to('Emil').format()
        self.assertEqual(res, 'GRANT READ ON db TO Emil')

        res = grant(PRIVILEGES.READ).to('Emil').format()
        self.assertEqual(res, 'GRANT READ PRIVILEGES TO Emil')
        
    def test_grant_privileges_write(self):
        res = grant(PRIVILEGES.WRITE).on('db').to('Emil').format()
        self.assertEqual(res, 'GRANT WRITE ON db TO Emil')

        res = grant(PRIVILEGES.WRITE).to('Emil').format()
        self.assertEqual(res, 'GRANT WRITE PRIVILEGES TO Emil')

    def test_awesome_syntax(self):
        self.assertEqual(grant().all.to('Emil').format(), 'GRANT ALL PRIVILEGES TO Emil')

        self.assertEqual(grant().read.to('Emil').format(), 'GRANT READ PRIVILEGES TO Emil')

        self.assertEqual(grant().write.to('Emil').format(), 'GRANT WRITE PRIVILEGES TO Emil')

