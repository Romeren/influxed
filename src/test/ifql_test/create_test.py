#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: create_test.py
 File Created: Monday, 18th February 2019 9:47:51 am
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import unittest
from influxed import create
from influxed import KEY_WORDS
from influxed.ifql.exceptions import MissingArgument


class SelectTest(unittest.TestCase):
    """
        CREATE RETENTION POLICY <retention_policy_name> ON <database_name> DURATION <duration> REPLICATION <n> [SHARD DURATION <duration>] [DEFAULT]
    
        CREATE DATABASE <database_name> [WITH [DURATION <duration>] [REPLICATION <n>] [SHARD DURATION <duration>] [NAME <retention-policy-name>]]
    """
    
    def test_create_retention_policy_missing_argument_name(self):
        with self.assertRaises(MissingArgument):
            create(KEY_WORDS.RETENTION_POLICIES).on('test').duration('1d').replication(1).format()

    def test_create_retention_policy_missing_argument_database(self):
        with self.assertRaises(MissingArgument):
            create(KEY_WORDS.RETENTION_POLICIES).name('test').duration('1d').replication(1).format()

    def test_create_retention_policy_missing_argument_replication(self):
        with self.assertRaises(MissingArgument):
            create(KEY_WORDS.RETENTION_POLICIES).name('test').on('test').duration('1d').format()

    def test_create_retention_policy_missing_argument_duration(self):
        with self.assertRaises(MissingArgument):
            create(KEY_WORDS.RETENTION_POLICIES).name('test').on('test').replication(1).format()

    def test_create_retention_policy(self):
        res = create(KEY_WORDS.RETENTION_POLICIES).name('test').on('db').duration('1d').replication(1).format()
        self.assertEqual(res, 'CREATE RETENTION POLICY test ON db DURATION 1d REPLICATION 1')
    
    def test_create_retention_policy_duration(self):
        res = create(KEY_WORDS.RETENTION_POLICIES).name('test').on('db').duration('4d').replication(1).format()
        self.assertEqual(res, 'CREATE RETENTION POLICY test ON db DURATION 4d REPLICATION 1')

        with self.assertRaises(ValueError):
            create(KEY_WORDS.RETENTION_POLICIES).name('test').on('db').duration('4test').replication(1).format()
    
        with self.assertRaises(ValueError):
            create(KEY_WORDS.RETENTION_POLICIES).name('test').on('db').duration('-1h').replication(1).format()

    def test_create_retention_policy_replication(self):
        res = create(KEY_WORDS.RETENTION_POLICIES).name('test').on('db').duration('1d').replication(9).format()
        self.assertEqual(res, 'CREATE RETENTION POLICY test ON db DURATION 1d REPLICATION 9')

        with self.assertRaises(ValueError):
            create(KEY_WORDS.RETENTION_POLICIES).name('test').on('db').duration('1d').replication(0).format()

        with self.assertRaises(ValueError):
            create(KEY_WORDS.RETENTION_POLICIES).name('test').on('db').duration('1d').replication(-5).format()


    def test_create_database_missing_argument_database(self):
        with self.assertRaises(MissingArgument):
            create(KEY_WORDS.DATABASES).format()

    def test_create_database(self):
        res = create(KEY_WORDS.DATABASES).name('test').format()
        self.assertEqual(res, 'CREATE DATABASE test')

    def test_create_database_duration(self):
        res = create(KEY_WORDS.DATABASES).name('test').duration('1d').format()
        self.assertEqual(res, 'CREATE DATABASE test WITH DURATION 1d')

    def test_create_database_replication(self):
        res = create(KEY_WORDS.DATABASES).name('test').replication(1).format()
        self.assertEqual(res, 'CREATE DATABASE test WITH REPLICATION 1')

    def test_create_database_shard_duration(self):
        res = create(KEY_WORDS.DATABASES).name('test').shard_duration('2d').format()
        self.assertEqual(res, 'CREATE DATABASE test WITH SHARD DURATION 2d')

    def test_create_database_retention_policy(self):
        res = create(KEY_WORDS.DATABASES).name('test').duration('1d').replication(1).shard_duration('2d').format()
        self.assertEqual(res, 'CREATE DATABASE test WITH DURATION 1d REPLICATION 1 SHARD DURATION 2d')


    def test_create_user_missing_argument_username(self):
        with self.assertRaises(MissingArgument):
            create(KEY_WORDS.USERS).password('test').format()

    def test_create_user_missing_argument_password(self):
        with self.assertRaises(MissingArgument):
            create(KEY_WORDS.USERS).username('test').format()

    def test_create_user(self):
        res = create(KEY_WORDS.USERS).username('test').password('test').format()
        self.assertEqual(res, "CREATE USER test WITH PASSWORD 'test'")
    

    def test_awesome_syntax(self):
        self.assertEqual(
            create().user.username('emil').password('test').format(),
            "CREATE USER emil WITH PASSWORD 'test'"
        )
