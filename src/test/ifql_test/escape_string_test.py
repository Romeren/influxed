
import unittest
import datetime as dt
from influxed.ifql.util import str_needs_escape


class SelectTest(unittest.TestCase):


    def test_needs_escape(self):
        tests = [
            '123',
            '1sf2',
            'a-2',
            'sdsffsd-',
            'ff-dd',
            'mmfd rfewr'
            'FROM a',
            'SELECT b',
            '( select * from b )',
            '(select * from b)',
            '(select a from b)',
            '21ashdbjas',
            'FROM',
            'from',
            'DELETE',
            'delete'
        ]

        for test in tests:
            self.assertTrue(str_needs_escape(test), test)


    def test_not_needs_escape(self):
        tests = [
            'qwertyu',
            'adfghj',
            'ASDGH',
            'ASDASDasdasdASD',
            'ffAAss',
            'mmfd123rfewr'
        ]

        for test in tests:
            self.assertFalse(str_needs_escape(test), test)

