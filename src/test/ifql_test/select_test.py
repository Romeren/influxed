#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: select_test.py
 File Created: Friday, 15th February 2019 5:34:46 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import unittest
import datetime as dt
from influxed import select
from influxed.ifql.column import Column, time
from influxed.ifql.functions import Count, Min, Max, Mean, Distinct, Percentile, Derivative, Sum, Stddev, First, Last, Median, Difference
from influxed.ifql.exceptions import MissingArgument


class SelectTest(unittest.TestCase):


    def test_missing_from_argument(self):
        with self.assertRaises(MissingArgument):
            select('a').format()

    def test_select(self):
        # Test default
        res = select().from_('d').format()
        self.assertEqual(res, 'SELECT * FROM d')

        res = select('a').from_('d').format()
        self.assertEqual(res, 'SELECT a FROM d')

        res = select().select('a').from_('d').format()
        self.assertEqual(res, 'SELECT a FROM d')

        res = select().select('a', 'b').from_('d').format()
        self.assertEqual(res, 'SELECT a, b FROM d')

        res = select().with_series('a', 'b', 'c').from_('d').format()
        self.assertEqual(res, 'SELECT a, b, c FROM d')

        # Test append more args
        res = select().with_series('a', 'b', 'c').from_('d').select('q').format()
        self.assertEqual(res, 'SELECT a, b, c, q FROM d')

        # Test overwrite args
        res = select().with_series('a', 'b', 'c').from_('d').with_series('q').format()
        self.assertEqual(res, 'SELECT q FROM d')
    
    def test_from(self):
        res = select('a').from_('b').format()
        self.assertEqual(res, 'SELECT a FROM b')

        res = select('a').from_('b').from_('c').format()
        self.assertEqual(res, 'SELECT a FROM c')
    
    def test_filter_by(self):
        res = select('a').from_('b').filter_by(
            a=1
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE a=1')
        res = select('a').from_('b').filter_by(
            a=1,
            b=5
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE a=1 AND b=5')
    
    def test_filter(self):
        col = Column('a')
        res = select(col).from_('b').filter(
            col > 5
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE a>5')

        res = select(col).from_('b').filter(
            col >= 5
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE a>=5')

        res = select(col).from_('b').filter(
            col < 5
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE a<5')

        res = select(col).from_('b').filter(
            col >= 5
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE a>=5')
        
        res = select(col).from_('b').filter(
            col == 5
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE a=5')

        res = select(col).from_('b').filter(
            col == 5,
            time >= dt.datetime(2019, 1, 1)
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE a=5 AND time >= \'2019-01-01 00:00:00.000\'')
        
        res = select(col).from_('b').filter(
            time >= dt.datetime(2019, 1, 1),
            time < dt.datetime(2019, 2, 1)
        ).format()
        self.assertEqual(res, 'SELECT a FROM b WHERE time >= \'2019-01-01 00:00:00.000\' AND time < \'2019-02-01 00:00:00.000\'')
        
    def test_group_by_missing_aggregation(self):
        with self.assertRaises(MissingArgument):
            col = Column('a')
            res = select(col).from_('b').group_by(time('1d')).format()
    
    def test_group_by(self):
        col = Column('a')
        
        expected = ["COUNT", "MIN", "MAX", "MEAN", "DISTINCT", "DERIVATIVE", "SUM", "STDDEV", "FIRST", "LAST"]
        funcs = [Count, Min, Max, Mean, Distinct, Derivative, Sum, Stddev, First, Last]
        for indx, func in enumerate(funcs):
            res = select(func(col)).from_('b').group_by(time('1d')).format()
            self.assertEqual(res, f'SELECT {expected[indx]}(a) FROM b GROUP BY time(1d)')
        
        res = select("a").from_('b').group_by(time('1d')).format()
        self.assertEqual(res, f'SELECT a FROM b GROUP BY time(1d)')

        res = select(Min("a")).from_('b').group_by(time('1d')).format()
        self.assertEqual(res, f'SELECT MIN(a) FROM b GROUP BY time(1d)')

    def test_group_by_column(self):
        col = Column('c')
        res = select('a').from_('b').group_by(col).format()
        expected = 'SELECT a FROM b GROUP BY c'
        self.assertEqual(res, expected)

    def test_group_by_str(self):
        res = select('a').from_('b').group_by('c').format()
        expected = 'SELECT a FROM b GROUP BY c'
        self.assertEqual(res, expected)

    def test_group_by_select_column_str(self):
        col = Column('a')
        res = select(col).from_('b').group_by('c').format()
        expected = 'SELECT a FROM b GROUP BY c'
        self.assertEqual(res, expected)

    def test_fill(self):
        res = select("a").from_('b').fill(1).format()
        self.assertEqual(res, f'SELECT a FROM b FILL(1)')
        res = select("a").from_('b').fill(-1).format()
        self.assertEqual(res, f'SELECT a FROM b FILL(-1)')
        res = select("a").from_('b').fill(67).format()
        self.assertEqual(res, f'SELECT a FROM b FILL(67)')
    
    def test_limit(self):
        res = select("a").from_('b').limit(1).format()
        self.assertEqual(res, f'SELECT a FROM b LIMIT 1')

        res = select("a").from_('b').limit(55).format()
        self.assertEqual(res, f'SELECT a FROM b LIMIT 55')

        with self.assertRaises(ValueError):
            res = select("a").from_('b').limit(0)
    

        with self.assertRaises(ValueError):
            res = select("a").from_('b').limit(-43)
        
    def test_order_by(self):
        res = select("a").from_('b').order_by('time').format()
        self.assertEqual(res, f'SELECT a FROM b ORDER BY time ASC')

        res = select("a").from_('b').order_by(time.descending).format()
        self.assertEqual(res, f'SELECT a FROM b ORDER BY time DESC')

        res = select("a").from_('b').order_by(time.desc).format()
        self.assertEqual(res, f'SELECT a FROM b ORDER BY time DESC')

        res = select("a").from_('b').order_by(time.ascending).format()
        self.assertEqual(res, f'SELECT a FROM b ORDER BY time ASC')

        res = select("a").from_('b').order_by(time.asc).format()
        self.assertEqual(res, f'SELECT a FROM b ORDER BY time ASC')
    
    def test_first(self):
        res = select("a").from_('b').first(1).format()
        self.assertEqual(res, f'SELECT a FROM b LIMIT 1')

        res = select("a").from_('b').first(55).format()
        self.assertEqual(res, f'SELECT a FROM b LIMIT 55')

        with self.assertRaises(ValueError):
            res = select("a").from_('b').first(0)
    

        with self.assertRaises(ValueError):
            res = select("a").from_('b').first(-43)
  
    def test_last(self):
        res = select("a").from_('b').last(1).format()
        self.assertEqual(res, f'SELECT a FROM b ORDER BY time DESC LIMIT 1')

        res = select("a").from_('b').last(55).format()
        self.assertEqual(res, f'SELECT a FROM b ORDER BY time DESC LIMIT 55')

        with self.assertRaises(ValueError):
            res = select("a").from_('b').last(0)
    

        with self.assertRaises(ValueError):
            res = select("a").from_('b').last(-43)

    def test_distinct(self):
        res = select(Distinct("a")).from_('b').format()
        self.assertEqual(res, f'SELECT DISTINCT(a) FROM b')

    def test_count(self):
        res = select(Count("a")).from_('b').format()
        self.assertEqual(res, f'SELECT COUNT(a) FROM b')

    def test_sum(self):
        res = select(Sum("a")).from_('b').format()
        self.assertEqual(res, f'SELECT SUM(a) FROM b')
    
    def test_min(self):
        res = select(Min("a")).from_('b').format()
        self.assertEqual(res, f'SELECT MIN(a) FROM b')
    
    def test_max(self):
        res = select(Max("a")).from_('b').format()
        self.assertEqual(res, f'SELECT MAX(a) FROM b')
    
    def test_mean(self):
        res = select(Mean("a")).from_('b').format()
        self.assertEqual(res, f'SELECT MEAN(a) FROM b')
    
    def test_median(self):
        res = select(Median("a")).from_('b').format()
        self.assertEqual(res, f'SELECT MEDIAN(a) FROM b')
    
    def test_percentile(self):
        # res = select(Percentile("a", 1)).from_('b').format()
        # self.assertEqual(res, f'SELECT PERCENTILE(a) FROM b')
        pass

    def test_derivative(self):
        res = select(Derivative("a")).from_('b').format()
        self.assertEqual(res, f'SELECT DERIVATIVE(a) FROM b')
    
    def test_difference(self):
        res = select(Difference("a")).from_('b').format()
        self.assertEqual(res, f'SELECT DIFFERENCE(a) FROM b')
    


    def test_apply_first(self):
        res = select(First("a")).from_('b').format()
        self.assertEqual(res, f'SELECT FIRST(a) FROM b')
    
    def test_apply_last(self):
        res = select(Last("a")).from_('b').format()
        self.assertEqual(res, f'SELECT LAST(a) FROM b')
    
