


#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: select_test.py
 File Created: Friday, 15th February 2019 5:34:46 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import unittest
import datetime as dt
from influxed import select
from influxed.ifql.column import Column, time, Field
from influxed.ifql.functions import Expression
from influxed.ifql.exceptions import MissingArgument


class ExpressionTest(unittest.TestCase):

    def test_minus_expression(self):
        res = select(Column('a') - 2).from_('d').format()
        self.assertEqual(res, 'SELECT a - 2 FROM d')

    def test_rminus_expression(self):
        res = select(2 - Column('a')).from_('d').format()
        self.assertEqual(res, 'SELECT 2 - a FROM d')

    def test_add_expression(self):
        res = select(Column('a') + 42).from_('d').format()
        self.assertEqual(res, 'SELECT a + 42 FROM d')

    def test_radd_expression(self):
        res = select(45 + Column('a')).from_('d').format()
        self.assertEqual(res, 'SELECT 45 + a FROM d')

    def test_multiply_expression(self):
        res = select(Column('a') * 3).from_('d').format()
        self.assertEqual(res, 'SELECT a * 3 FROM d')

    def test_rmultiply_expression(self):
        res = select(6 * Column('a')).from_('d').format()
        self.assertEqual(res, 'SELECT 6 * a FROM d')

    def test_multiply_expression(self):
        res = select(Column('a') / 2).from_('d').format()
        self.assertEqual(res, 'SELECT a / 2 FROM d')

    def test_rmultiply_expression(self):
        res = select(1.2 / Column('a')).from_('d').format()
        self.assertEqual(res, 'SELECT 1.2 / a FROM d')

    def test_column_on_column_expression(self):
        res = select(Column('a') - Column('b')).from_('d').format()
        self.assertEqual(res, 'SELECT a - b FROM d')

    def test_nested_expressions(self):
        res = select((1.2 / Column('a')) * 6).from_('d').format()
        self.assertEqual(res, 'SELECT (1.2 / a) * 6 FROM d')

    def test_nested_column_on_column_expression(self):
        res = select(Column('a') - Column('b') / (2 * Column("c")) ).from_('d').format()
        self.assertEqual(res, 'SELECT a - (b / (2 * c)) FROM d')


    def test_nested_column_on_func_expression(self):
        res = select(Field('a').mean() - Field('b') / (2 * Field("c").max()) ).from_('d').format()
        self.assertEqual(res, 'SELECT MEAN(a) - (b / (2 * MAX(c))) FROM d')





