#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: server_test.py
 File Created: Monday, 25th February 2019 9:27:53 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import os
import unittest
import requests
from influxed.orm.engine import Engine
from influxed.orm.database import Database
from influxed.ifql import select, show


class DatabaseTest(unittest.TestCase):
    """
    Requires environment variable:
        - connection_string
        - username
        - password
    """

    def setUp(self):
        db = Database()
        db.name = "Test"
        self.db = db
        self.engine = Engine()
        self.engine.add_server(
            os.getenv("connection_string"),
            os.getenv("username"),
            os.getenv("password"),
            client=requests,
            databases=[db],
        )

    def test_has_show(self):
        self.assertTrue(hasattr(list(self.engine.server.databases.values())[0], "show"))
        self.assertIsInstance(self.db.show(), show)
        # self.assertIsInstance(list(self.engine.server.databases.values())[0].query, show_statement_builder)

    def test_has_insert(self):
        self.assertTrue(
            hasattr(list(self.engine.server.databases.values())[0], "insert")
        )

    def test_has_query(self):
        self.assertTrue(
            hasattr(list(self.engine.server.databases.values())[0], "query")
        )

        self.assertIsInstance(
            list(self.engine.server.databases.values())[0].query, select
        )
