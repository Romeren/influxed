#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: engine_test.py
 File Created: Monday, 25th February 2019 8:56:32 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import unittest
import os
from influxed.orm.engine import Engine
from influxed.orm.columns.fieldkey import FieldKey
from influxed.orm.columns.tagkey import TagKey
from influxed.orm.declarative.declarative_base import DecarativeBase

from test.orm_test.influx_server_mock import InfluxServerMock


class DeclarativeTest(unittest.TestCase):

    def test_declarative_measurement_missing_measurement_name(self):
        class test_measurement(DecarativeBase):
            pass

        e = Engine(influx_server_cls=InfluxServerMock)

        with self.assertRaises(ValueError):
            e.add_server(
                os.getenv("connection_string"),
                os.getenv("username"),
                os.getenv("password"),
                declarative_base=DecarativeBase,
            )

    def test_declarative_measurement_missing_database_name(self):
        class test_measurement(DecarativeBase):
            __measurement__ = "TestMeasurement"

        InfluxServerMock.__rp_names__ = [("autogen", "test1"), ("autogen", "test2")]
        e = Engine(influx_server_cls=InfluxServerMock)

        with self.assertRaises(ValueError):
            e.add_server(
                os.getenv("connection_string"),
                os.getenv("username"),
                os.getenv("password"),
                declarative_base=DecarativeBase,
            )

    def test_declarative_measurement(self):
        class test_measurement(DecarativeBase):
            __measurement__ = "TestMeasurement"

        InfluxServerMock.__rp_names__ = [("autogen", "test")]
        e = Engine(influx_server_cls=InfluxServerMock)

        e.add_server(
            os.getenv("connection_string"),
            os.getenv("username"),
            os.getenv("password"),
            declarative_base=DecarativeBase,
        )

        self.assertIsNotNone(e.server)
        self.assertIsNotNone(e.server.test)
        self.assertIsNotNone(e.server.test.autogen.TestMeasurement)

    def test_declarative_measurement_key(self):
        class test_measurement(DecarativeBase):
            __measurement__ = "TestMeasurement"
            test_field = FieldKey
            test_tag = TagKey
            another_field = FieldKey

        InfluxServerMock.__rp_names__ = [("autogen", "test")]
        e = Engine(influx_server_cls=InfluxServerMock)

        e.add_server(
            os.getenv("connection_string"),
            os.getenv("username"),
            os.getenv("password"),
            declarative_base=DecarativeBase,
        )

        self.assertIsNotNone(e.server)
        self.assertIsNotNone(e.server.test)
        self.assertIsNotNone(e.server.test.autogen.TestMeasurement)
        self.assertIsNotNone(e.server.test.autogen.TestMeasurement.test_field)
        self.assertIsInstance(
            e.server.test.autogen.TestMeasurement.test_field, FieldKey
        )
        self.assertIsNotNone(e.server.test.autogen.TestMeasurement.test_field.database)
        self.assertIsNotNone(
            e.server.test.autogen.TestMeasurement.test_field.measurement
        )
        self.assertIsNotNone(e.server.test.autogen.TestMeasurement.test_field.database)
        self.assertIsNotNone(e.server.test.autogen.TestMeasurement.test_field.rp)

        self.assertIsInstance(e.server.test.autogen.TestMeasurement.test_tag, TagKey)

        self.assertIsInstance(
            e.server.test.autogen.TestMeasurement.another_field, FieldKey
        )

        self.assertTrue(
            hasattr(e.server.test.autogen.TestMeasurement.test_field, "query")
        )
