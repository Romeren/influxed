#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: engine_test.py
 File Created: Monday, 25th February 2019 8:56:32 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import asyncio
import os
import requests
from tornado.httpclient import HTTPClient, AsyncHTTPClient
import unittest
from influxed.orm.engine import Engine


class EngineTest(unittest.TestCase):
    """
        Important, these tests only run if you have a connection string, username and a password
        in your env.
    """

    def test_wrong_credentials(self):
        """
            Test error on wrong credentials
        """
        
        with self.assertRaises(ConnectionRefusedError):
            Engine().add_server(
                'http://localhost', 
                'shit',  # If this fails, and it is because this actually is your username and password...
                'stuff', #  then please do raise a ticket so we can get you to a doctor and get you checked
                reflect=True
            )


    def test_connect(self):
        """
            Test show databases
        """
        e = Engine()
        e.add_server(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password')
        )
        self.assertIsNotNone(e.server)
        self.assertIsNotNone(e.servers.get(os.getenv('username'), None))
        self.assertEqual(len(e.servers.keys()), 1)

    def test_disconnect(self):
        """
            Test show databases
        """
        e = Engine()
        e.add_server(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password')
        )
        self.assertEqual(len(e.servers.keys()), 1)
        e.remove_server(os.getenv('username'))
        
        self.assertEqual(len(e.servers.keys()), 0)


    def test_add_server(self):
        e = Engine()

        e.add_server(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password')
        )
        self.assertEqual(len(e.servers.keys()), 1)
    
    def test_remove_server(self):
        e = Engine()

        e.add_server(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password')
        )

        e.remove_server(
            os.getenv('username'),
        )
        self.assertEqual(len(e.servers.keys()), 0)

    def test_engine_reflect(self):
        e = Engine()

        e.add_server(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            reflect=True
        )
