#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: server_test.py
 File Created: Monday, 25th February 2019 9:27:53 pm
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""

import os
import unittest
import requests
from influxed.orm.engine import Engine


class ServerTest(unittest.TestCase):
    """
        Requires environment variable:
            - connection_string
            - username
            - password
    """
    
    def setUp(self):
        self.engine = Engine()
        self.engine.add_server(
            os.getenv('connection_string'),
            os.getenv('username'),
            os.getenv('password'),
            client=requests
        )

    def test_has_show(self):
        self.assertTrue(hasattr(self.engine.server, 'show'))
