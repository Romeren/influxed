#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
 File: server_mock.py
 File Created: Wednesday, 20th March 2019 9:13:03 am
 Author: ESR - Romeren (emilromer@hotmail.com)
 -----
 Copyright 2019 OpenSourced, OpenSourced
 -----
 Last Modified:
 Date	By	Comments
 -----
"""
import pandas as pd
from influxed.orm.influx_server import InfluxServer


class InfluxServerMock(InfluxServer):
    __rp_names__ = []

    def __look_up_databases_and_rps__(self):
        return pd.DataFrame(
            data=[(db, rp) for (rp, db) in self.__rp_names__],
            columns=["database_name", "rp_name"],
        )
